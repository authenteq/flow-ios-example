  //
  //  ViewController.m
  //  Authenteq Example ObjC
  //
  //  Created by Vitaliy Gozhenko on 21.02.2020.
  //  Copyright © 2020 Authenteq. All rights reserved.
  //

#import "ViewController.h"
#import "License.h"
#import "IdentificationResult+Representation.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIButton *selfieAuthenticationButton;
@property (nonatomic) NSString * _Nullable userId;

@end

@implementation ViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  [self customizeAuthenteqSdkTheme];
}

- (IBAction)identification:(id)sender {
  UIViewController *viewController =
  [[AuthenteqFlow instance]
   identificationViewControllerWith:AuthenteqLicenseKey
   documents:
   @[
     @[
       AuthenteqIdDocumentType.passport,
       AuthenteqIdDocumentType.driversLicense,
       AuthenteqIdDocumentType.idCard
     ]
   ]
   delegate:self];
  [self presentViewController:viewController animated:true completion:nil];
}

- (IBAction)selfieAuthentication:(id)sender {
  UIViewController *viewController =
  [[AuthenteqFlow instance]
   selfieAuthenticationViewControllerWith:AuthenteqLicenseKey
   userId: self.userId
   delegate:self];
  [self presentViewController:viewController animated:true completion:nil];
}

- (void)customizeAuthenteqSdkTheme {
  AuthenteqFlowTheme *theme = [[AuthenteqFlowTheme alloc]
                               initWithPrimaryColor:UIColor.orangeColor
                               textColor:UIColor.blackColor
                               screenBackgroundColor:UIColor.whiteColor
                               font:[UIFont systemFontOfSize:1]
                               boldFont:[UIFont boldSystemFontOfSize:1]
                               identificationInstructionImageForSelfie:nil
                               identificationInstructionImageForPassport:nil
                               identificationInstructionImageForDriverLicense:nil
                               identificationInstructionImageForIdCard:nil];
  [[AuthenteqFlow instance] setTheme: theme];
}

#pragma MARK - AuthenteqIdentificationDelegate

- (void)authenteqDidFinishIdentificationWith:(IdentificationResult *)result {
  [self.presentedViewController dismissViewControllerAnimated:true completion:nil];
  self.userId = result.userId;
  [self.selfieAuthenticationButton setEnabled:YES];
  NSLog(@"Did finish identification with result:\n%@", result.textRepresentation);
}

- (void)authenteqDidFailIdentificationWith:(enum AuthenteqFlowError)error {
  [self.presentedViewController dismissViewControllerAnimated:true completion:nil];
  NSLog(@"Did finish identification error: %ld", (long) error);
}

#pragma MARK - AuthenteqSelfieAuthentificationDelegate

- (void)authenteqDidFinishSelfieAuthenticationWith:(BOOL)result {
  [self.presentedViewController dismissViewControllerAnimated:true completion:nil];
  NSString *text = (result) ? @"true" : @"false";
  NSLog(@"Did finish identification with result:\n%@", text);
}

- (void)authenteqDidFailSelfieAuthenticationWith:(enum AuthenteqFlowError)error {
  [self.presentedViewController dismissViewControllerAnimated:true completion:nil];
  NSLog(@"Did finish identification error: %ld", (long) error);
}

@end
