//
//  IdentificationResult+Representation.h
//  Authenteq Example ObjC
//
//  Created by Vitaliy Gozhenko on 24.02.2020.
//  Copyright © 2020 Authenteq. All rights reserved.
//

#import <Foundation/Foundation.h>
@import AuthenteqFlow;

NS_ASSUME_NONNULL_BEGIN

@interface IdentificationResult (Representation)

- (NSString *)textRepresentation;
- (NSDictionary *)dictionaryRepresentation;

@end

NS_ASSUME_NONNULL_END
