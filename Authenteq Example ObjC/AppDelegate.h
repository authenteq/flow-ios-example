//
//  AppDelegate.h
//  Authenteq Example ObjC
//
//  Created by Vitaliy Gozhenko on 21.02.2020.
//  Copyright © 2020 Authenteq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic) UIWindow *window;

@end

