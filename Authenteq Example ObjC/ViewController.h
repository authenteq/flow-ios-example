//
//  ViewController.h
//  Authenteq Example ObjC
//
//  Created by Vitaliy Gozhenko on 21.02.2020.
//  Copyright © 2020 Authenteq. All rights reserved.
//

#import <UIKit/UIKit.h>
@import AuthenteqFlow;

@interface ViewController : UIViewController
<AuthenteqIdentificationDelegate, AuthenteqSelfieAuthentificationDelegate>


@end

