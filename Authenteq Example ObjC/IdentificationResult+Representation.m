//
//  IdentificationResult+Representation.m
//  Authenteq Example ObjC
//
//  Created by Vitaliy Gozhenko on 24.02.2020.
//  Copyright © 2020 Authenteq. All rights reserved.
//

#import "IdentificationResult+Representation.h"

@implementation IdentificationResult (Representation)

- (NSDictionary *)dictionaryRepresentation {
  NSMutableDictionary *fields = [NSMutableDictionary new];
  [fields setObject:self.userId forKey:@"userId"];
  [fields setObject:[self objectOrNull:self.recoveryWords] forKey:@"recoveryWords"];
  [fields setObject:[self objectOrNull:self.selfieImageFilePath] forKey:@"selfieImageFilePath"];
  for (DocumentIdentificationResult *document in self.documents) {
    [fields setObject:[self objectOrNull:document.documentType] forKey:@"documentType"];
    if ([document isKindOfClass:[PassportIdentificationResult class]]) {
      [fields addEntriesFromDictionary:[self passportFieldsFrom:(PassportIdentificationResult *) document]];
    } else if ([document isKindOfClass:[IdCardIdentificationResult class]]) {
      [fields addEntriesFromDictionary:[self idCardFieldsFieldsFrom:(IdCardIdentificationResult *) document]];
    } else if ([document isKindOfClass:[DriversLicenseIdentificationResult class]]) {
      [fields addEntriesFromDictionary:[self driversLicenseFieldsFrom:(DriversLicenseIdentificationResult *) document]];
    } else {
      [NSException raise:@"Unhandled document type" format:@"Unhandled document type"];
    }
  }
  return fields;
}

- (NSString *)textRepresentation {
  NSMutableArray *array = [NSMutableArray new];
  [self.dictionaryRepresentation
   enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, NSObject * _Nullable value, BOOL * _Nonnull stop) {
    [array addObject:[NSString stringWithFormat:@"%@: %@", key, [value debugDescription]]];
  }];
  return [array componentsJoinedByString:@"\n"];
}

- (NSDictionary *)passportFieldsFrom:(PassportIdentificationResult *) document {
  NSMutableDictionary *fields = [NSMutableDictionary new];
	[fields setObject:[self objectOrNull:document.dateOfBirth] forKey:@"dateOfBirth"];
  [fields setObject:[self objectOrNull:document.dateOfExpiry] forKey:@"dateOfExpiry"];
  [fields setObject:[self objectOrNull:document.dateOfIssue] forKey:@"dateOfIssue"];
  [fields setObject:[self objectOrNull:document.documentImageFilePath] forKey:@"documentImageFilePath"];
  [fields setObject:[self objectOrNull:document.documentNumber] forKey:@"documentNumber"];
  [fields setObject:[self objectOrNull:document.givenNames] forKey:@"givenNames"];
  [fields setObject:[self objectOrNull:document.isEighteenPlus] forKey:@"isEighteenPlus"];
  [fields setObject:[self objectOrNull:document.isSixteenPlus] forKey:@"isSixteenPlus"];
  [fields setObject:[self objectOrNull:document.issuingCountry] forKey:@"issuingCountry"];
  [fields setObject:[self objectOrNull:document.isTwentyOnePlus] forKey:@"isTwentyOnePlus"];
  [fields setObject:[self objectOrNull:document.nationality] forKey:@"nationality"];
  [fields setObject:[self objectOrNull:document.sex] forKey:@"sex"];
  [fields setObject:[self objectOrNull:document.surname] forKey:@"surname"];
  return fields;
}

- (NSDictionary *)idCardFieldsFieldsFrom:(IdCardIdentificationResult *) document {
  NSMutableDictionary *fields = [NSMutableDictionary new];
  [fields setObject:[self objectOrNull:document.dateOfBirth] forKey:@"dateOfBirth"];
  [fields setObject:[self objectOrNull:document.dateOfExpiry] forKey:@"dateOfExpiry"];
  [fields setObject:[self objectOrNull:document.dateOfIssue] forKey:@"dateOfIssue"];
  [fields setObject:[self objectOrNull:document.documentBackImageFilePath] forKey:@"documentBackImageFilePath"];
  [fields setObject:[self objectOrNull:document.documentFrontImageFilePath] forKey:@"documentFrontImageFilePath"];
  [fields setObject:[self objectOrNull:document.documentNumber] forKey:@"documentNumber"];
  [fields setObject:[self objectOrNull:document.givenNames] forKey:@"givenNames"];
  [fields setObject:[self objectOrNull:document.isEighteenPlus] forKey:@"isEighteenPlus"];
  [fields setObject:[self objectOrNull:document.isSixteenPlus] forKey:@"isSixteenPlus"];
  [fields setObject:[self objectOrNull:document.issuingCountry] forKey:@"issuingCountry"];
  [fields setObject:[self objectOrNull:document.isTwentyOnePlus] forKey:@"isTwentyOnePlus"];
  [fields setObject:[self objectOrNull:document.nationality] forKey:@"nationality"];
  [fields setObject:[self objectOrNull:document.sex] forKey:@"sex"];
  [fields setObject:[self objectOrNull:document.surname] forKey:@"surname"];
  return fields;
}

- (NSDictionary *)driversLicenseFieldsFrom:(DriversLicenseIdentificationResult *) document {
  NSMutableDictionary *fields = [NSMutableDictionary new];
  [fields setObject:[self objectOrNull:document.dateOfBirth] forKey:@"dateOfBirth"];
  [fields setObject:[self objectOrNull:document.dateOfExpiry] forKey:@"dateOfExpiry"];
  [fields setObject:[self objectOrNull:document.dateOfIssue] forKey:@"dateOfIssue"];
  [fields setObject:[self objectOrNull:document.documentBackImageFilePath] forKey:@"documentBackImageFilePath"];
  [fields setObject:[self objectOrNull:document.documentFrontImageFilePath] forKey:@"documentFrontImageFilePath"];
  [fields setObject:[self objectOrNull:document.documentNumber] forKey:@"documentNumber"];
  [fields setObject:[self objectOrNull:document.givenNames] forKey:@"givenNames"];
  [fields setObject:[self objectOrNull:document.isEighteenPlus] forKey:@"isEighteenPlus"];
  [fields setObject:[self objectOrNull:document.isSixteenPlus] forKey:@"isSixteenPlus"];
  [fields setObject:[self objectOrNull:document.issuingCountry] forKey:@"issuingCountry"];
  [fields setObject:[self objectOrNull:document.isTwentyOnePlus] forKey:@"isTwentyOnePlus"];
  [fields setObject:[self objectOrNull:document.jurisdiction] forKey:@"jurisdiction"];
  [fields setObject:[self objectOrNull:document.licenseClass] forKey:@"licenseClass"];
  [fields setObject:[self objectOrNull:document.surname] forKey:@"surname"];
  [document.licenseClassDetails
   enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, DriversLicenseClassDetailResult * _Nonnull value, BOOL * _Nonnull stop) {
    NSString *keyName = [@"licenseClass" stringByAppendingString:key];
    [fields setObject:[self objectOrNull:value.from] forKey:[keyName stringByAppendingString:@"From"]];
    [fields setObject:[self objectOrNull:value.to] forKey:[keyName stringByAppendingString:@"To"]];
    [fields setObject:[self objectOrNull:value.notes] forKey:[keyName stringByAppendingString:@"Notes"]];
  }];
  return fields;
}

- (id)objectOrNull:(id) object {
  return object ? object : NSNull.null;
}

@end
