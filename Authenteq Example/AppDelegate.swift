//
//  AppDelegate.swift
//  Authenteq Example
//
//  Created by Vitaliy Gozhenko on 21.02.2020.
//  Copyright © 2020 Authenteq. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    return true
  }

}

