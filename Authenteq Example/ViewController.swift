//
//  ViewController.swift
//  Authenteq Example
//
//  Created by Vitaliy Gozhenko on 21.02.2020.
//  Copyright © 2020 Authenteq. All rights reserved.
//

import UIKit
import AuthenteqFlow

class ViewController: UIViewController {

  var userId: String?
  @IBOutlet var selfieAuthenticationButton: UIButton!

  override func viewDidLoad() {
    super.viewDidLoad()
    customizeAuthenteqSdkTheme()
  }

  @IBAction func identification(_ sender: Any) {
    let viewController = AuthenteqFlow.instance.identificationViewController(
      with: AuthenteqLicenseKey,
      documents: [[.passport, .driversLicense, .idCard]],
      delegate: self
    )
    viewController.modalPresentationStyle = .fullScreen
    present(viewController, animated: true)
  }

  @IBAction func selfieAuthentication(_ sender: Any) {
    guard let userId = userId else {
      print("You need to get userId from identification process to start selfie authentication")
      return
    }
    let viewController = AuthenteqFlow.instance.selfieAuthenticationViewController(
      with: AuthenteqLicenseKey,
      userId: userId,
      delegate: self
    )
    viewController.modalPresentationStyle = .fullScreen
    present(viewController, animated: true)
  }

  private func customizeAuthenteqSdkTheme() {
    AuthenteqFlow.instance.theme = AuthenteqFlowTheme(
      primaryColor: .orange,
      textColor: .black,
      screenBackgroundColor: .white,
      font: .systemFont(ofSize: 1),
      boldFont: .boldSystemFont(ofSize: 1),
      identificationInstructionImageForSelfie: nil,
      identificationInstructionImageForPassport: nil,
      identificationInstructionImageForDriverLicense: nil,
      identificationInstructionImageForIdCard: nil
    )
  }
}

extension ViewController: AuthenteqIdentificationDelegate {

  func authenteqDidFinishIdentification(with result: IdentificationResult) {
    presentedViewController?.dismiss(animated: true)
    userId = result.userId
    selfieAuthenticationButton.isEnabled = true
    print("Did finish identification with result:")
    print(result.textRepresentation)
  }

  func authenteqDidFailIdentification(with error: AuthenteqFlowError) {
    presentedViewController?.dismiss(animated: true)
    print("Did fail identification with error: \(error)")
  }
}

extension ViewController: AuthenteqSelfieAuthentificationDelegate {

  func authenteqDidFinishSelfieAuthentication(with result: Bool) {
    presentedViewController?.dismiss(animated: true)
    print("Did finish selfie authentication with result: \(String(describing: result))")
  }

  func authenteqDidFailSelfieAuthentication(with error: AuthenteqFlowError) {
    presentedViewController?.dismiss(animated: true)
    print("Did fail identification with error: \(error)")
  }
}
