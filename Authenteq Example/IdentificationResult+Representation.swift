//
//  IdentificationResult+Representation.swift
//  Authenteq Example
//
//  Created by Vitaliy Gozhenko on 24.02.2020.
//  Copyright © 2020 Authenteq. All rights reserved.
//

import UIKit
import AuthenteqFlow

extension IdentificationResult {

  var dictionaryRepresentation: [String: Any?] {
    var fields = [String: Any?]()
    fields["userId"] = userId
    fields["recoveryWords"] = recoveryWords
    fields["selfieImageFilePath"] = selfieImageFilePath
    for document in documents {
      fields["documentType"] = document.documentType
      switch document {
      case let document as PassportIdentificationResult:
        fields.merge(passportFields(from: document), uniquingKeysWith: {_, _ in})
      case let document as IdCardIdentificationResult:
        fields.merge(idCardFields(from: document), uniquingKeysWith: {_, _ in})
      case let document as DriversLicenseIdentificationResult:
        fields.merge(driversLicenseFields(from: document), uniquingKeysWith: {_, _ in})
      default:
        fatalError("Unhandled document type")
      }
    }
    return fields
  }

  var textRepresentation: String {
    dictionaryRepresentation
      .map { $0.key + ": " + String(describing: $0.value ?? "null") }
      .joined(separator: "\n")

  }

  private func passportFields(from document: PassportIdentificationResult) -> [String: Any?] {
    var fields = [String: Any?]()
    fields["dateOfBirth"] = document.dateOfBirth
    fields["dateOfExpiry"] = document.dateOfExpiry
    fields["dateOfIssue"] = document.dateOfIssue
    fields["documentImageFilePath"] = document.documentImageFilePath
    fields["documentNumber"] = document.documentNumber
    fields["givenNames"] = document.givenNames
    fields["isEighteenPlus"] = document.isEighteenPlus
    fields["isSixteenPlus"] = document.isSixteenPlus
    fields["issuingCountry"] = document.issuingCountry
    fields["isTwentyOnePlus"] = document.isTwentyOnePlus
    fields["nationality"] = document.nationality
    fields["sex"] = document.sex
    fields["surname"] = document.surname
    return fields
  }

  private func idCardFields(from document: IdCardIdentificationResult) -> [String: Any?] {
    var fields = [String: Any?]()
    fields["dateOfBirth"] = document.dateOfBirth
    fields["dateOfExpiry"] = document.dateOfExpiry
    fields["dateOfIssue"] = document.dateOfIssue
    fields["documentBackImageFilePath"] = document.documentBackImageFilePath
    fields["documentFrontImageFilePath"] = document.documentFrontImageFilePath
    fields["documentNumber"] = document.documentNumber
    fields["givenNames"] = document.givenNames
    fields["isEighteenPlus"] = document.isEighteenPlus
    fields["isSixteenPlus"] = document.isSixteenPlus
    fields["issuingCountry"] = document.issuingCountry
    fields["isTwentyOnePlus"] = document.isTwentyOnePlus
    fields["nationality"] = document.nationality
    fields["sex"] = document.sex
    fields["surname"] = document.surname
    return fields
  }

  private func driversLicenseFields(from document: DriversLicenseIdentificationResult) -> [String: Any?] {
    var fields = [String: Any?]()
    fields["dateOfBirth"] = document.dateOfBirth
    fields["dateOfExpiry"] = document.dateOfExpiry
    fields["dateOfIssue"] = document.dateOfIssue
    fields["documentBackImageFilePath"] = document.documentBackImageFilePath
    fields["documentFrontImageFilePath"] = document.documentFrontImageFilePath
    fields["documentNumber"] = document.documentNumber
    fields["givenNames"] = document.givenNames
    fields["isEighteenPlus"] = document.isEighteenPlus
    fields["isSixteenPlus"] = document.isSixteenPlus
    fields["issuingCountry"] = document.issuingCountry
    fields["isTwentyOnePlus"] = document.isTwentyOnePlus
    fields["jurisdiction"] = document.jurisdiction
    fields["licenseClass"] = document.licenseClass
    fields["surname"] = document.surname
    if let licenseClassDetails = document.licenseClassDetails {
      for (key, value) in licenseClassDetails {
        let keyName = "licenseClass" + key
        fields[keyName + "From"] = value.from
        fields[keyName + "To"] = value.to
        fields[keyName + "Notes"] = value.notes
      }
    }
    return fields
  }

}
