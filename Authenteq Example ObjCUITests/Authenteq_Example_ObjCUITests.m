  //
  //  Authenteq_Example_ObjCUITests.m
  //  Authenteq Example ObjCUITests
  //
  //  Created by Vitaliy Gozhenko on 21.02.2020.
  //  Copyright © 2020 Authenteq. All rights reserved.
  //

#import <XCTest/XCTest.h>

@interface Authenteq_Example_ObjCUITests : XCTestCase

@end

@implementation Authenteq_Example_ObjCUITests

- (void)setUp {
  self.continueAfterFailure = NO;
}

- (void)testAuthenteqSdkInitialization {
  XCUIApplication *app = [[XCUIApplication alloc] init];
  [app launch];
  [app.buttons[@"Identification"] tap];
  XCTAssertTrue(
                [app.buttons[@"Start Identification"] waitForExistenceWithTimeout:5],
                @"Authenteq SDK initialization failed"
                );
  [app.buttons[@"Start Identification"] tap];
  XCTAssertTrue([app.buttons[@"Take a Photo"] exists]);
}

@end
