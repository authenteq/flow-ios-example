//
//  Authenteq_ExampleUITests.swift
//  Authenteq ExampleUITests
//
//  Created by Vitaliy Gozhenko on 21.02.2020.
//  Copyright © 2020 Authenteq. All rights reserved.
//

import XCTest

class AuthenteqExampleUITests: XCTestCase {

  override func setUp() {
    continueAfterFailure = false
  }

  func testAuthenteqSdkInitialization() {
    let app = XCUIApplication()
    app.launch()
    app.buttons["Identification"].tap()
    XCTAssertTrue(
      app.buttons["Start Identification"].waitForExistence(timeout: 5),
      "Authenteq SDK initialization failed"
    )
    app.buttons["Start Identification"].tap()
    XCTAssertTrue(app.buttons["Take a Photo"].exists)
  }

}
